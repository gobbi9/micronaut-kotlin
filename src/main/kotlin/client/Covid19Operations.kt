package client

import io.micronaut.http.annotation.Get
import io.micronaut.validation.Validated
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import model.Covid19Stats
import io.reactivex.rxjava3.core.Single

@Validated
interface Covid19Operations {
    @Get suspend fun getStatsByCountry(@NotBlank country: String): Covid19Stats
    @Get fun getStatsByCountryRxJava(@NotBlank country: String): Single<Covid19Stats>
}
