package client

import io.micronaut.http.client.annotation.Client
import io.micronaut.http.annotation.RequestAttribute
import model.Covid19Stats
import io.reactivex.rxjava3.core.Single

@Client("https://covid19-api.weedmark.systems/api/v1/stats")
interface Covid19Client : Covid19Operations {
    override suspend fun getStatsByCountry(@RequestAttribute country: String): Covid19Stats
    override fun getStatsByCountryRxJava(@RequestAttribute country: String): Single<Covid19Stats>
}