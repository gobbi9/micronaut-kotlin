package model

data class Covid19Stats (
    val error: Boolean,
    val statusCode: Long,
    val message: String,
    val data: Covid19StatsData
)

data class Covid19StatsData (
    val lastChecked: String,
    val covid19Stats: List<Covid19Stat>
)

data class Covid19Stat (
    val city: String? = null,
    val province: String? = null,
    val country: String,
    val lastUpdate: String,
    val keyID: String? = null,
    val confirmed: Long,
    val deaths: Long,
    val recovered: Any? = null
)