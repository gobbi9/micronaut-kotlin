package controller

import io.micronaut.http.annotation.Controller
import client.Covid19Operations
import model.Covid19Stats
import jakarta.inject.Inject
import client.Covid19Client
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Get
import java.time.ZonedDateTime
import io.micronaut.http.annotation.QueryValue
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.concurrent.TimeUnit

@Controller("/covid19")
open class Covid19Controller {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @Inject lateinit var covid19Client: Covid19Client

    /*
    // parallel
    override suspend fun getStatsByCountry(country: String): Covid19Stats = coroutineScope {
        val stats1 = async { covid19Client.getStatsByCountry(country) }
        val stats2 = async { covid19Client.getStatsByCountry(country) }

        stats1.await()
    }
    */

    /*
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    MediaType.APPLICATION_JSON_STREAM blocks the response, waiting for all elements and return them all
    at once, which makes this useless. We could just do List<> in this case instead
    */

    // returns a list
    @Get("many")
    suspend fun getStatsByCountryParallel(@QueryValue country: String): List<Covid19Stats> = coroutineScope {
        (1..3)
            .map{ _ -> async(start = CoroutineStart.LAZY) { covid19Client.getStatsByCountry(country) } }
            .awaitAll()
            //.flatMapMerge {_ -> flowOf(covid19Client.getStatsByCountry(country))}
            // create a list of Deferred<Covid19Stats> and cal awaitAll
            // test with 3, 20 and 100, to see if the event loop thread limit is reached
    }

    // use http -S :8080/many#?country=Brazil
    // to test streams or curl -N ...

    // returns a flow
    // parallel
    @Get(uri = "many2", processes = [MediaType.TEXT_EVENT_STREAM])
    fun getStatsByCountryFlow2(@QueryValue country: String): Flow<Long> = flow {
        coroutineScope {
            // this does not create 100 threads, there is a threadpool aka event loop group thread limit
            (1..100).map { _ ->
                async { covid19Client.getStatsByCountry(country) }
            }
            .awaitAll()
            .forEach { emit(it.data.covid19Stats[0].confirmed) }
        }
    }

    // sequential
    @Get(uri = "many3", processes = [MediaType.TEXT_EVENT_STREAM])
    fun getStatsByCountryFlow3(@QueryValue country: String): Flow<Long> = 
        (1..10).asFlow().map { _ ->  covid19Client.getStatsByCountry(country)}
        .map {it.data.covid19Stats[0].confirmed}

    // parallel with delay 
    @Get(uri = "many4", processes = [MediaType.TEXT_EVENT_STREAM])
    fun getStatsByCountryFlow4(@QueryValue country: String): Flow<Long> = flow {
        coroutineScope {
            (1..10).map { _ ->
                async { covid19Client.getStatsByCountry(country) }
            }
            .awaitAll()
        }.forEach { emit(it.data.covid19Stats[0].confirmed)
            .also {  logger.info("Received"); delay(500) } }
    }

    // wait if we want to fire parallel requests but return the values as soon as they complete?
    // not possible with kotlin coroutines as of now, see https://github.com/Kotlin/kotlinx.coroutines/issues/1147
    // with RxJava the solution is trivial
    @Get(uri = "many5", processes = [MediaType.TEXT_EVENT_STREAM])
    fun getStatsByCountryFlow5(@QueryValue country: String): Observable<Long> = 
        Observable.range(0, 10)
            // the interleaving nature of flatMap can be observed by replacing 10 with 500 for example
            .flatMapSingle {_ -> covid19Client.getStatsByCountryRxJava(country)}
            .map { it -> it.data.covid19Stats[0].confirmed }
            // to make the debug easier
            .concatMap { it -> Observable.just(it).delay(250, TimeUnit.MILLISECONDS)
                .also {logger.info("Sending...")} }

    @Get(uri = "/stream", processes = [MediaType.TEXT_EVENT_STREAM])
    fun streamHeadlinesWithFlow(): Flow<String> = flow {
        repeat(100) {
            val text = "Latest Headline at ${ZonedDateTime.now()}"
            emit(text)
            delay(1_000)
        }
    }

}
